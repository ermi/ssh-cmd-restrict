#!/usr/bin/python3

#  ssh-cmd-restrict: Restrict SSH commands with key file and json config
#  Copyright (C) 2024  ermi
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime as dt
import argparse
import json
import sys
import os
import re

def check(fname, command):
	with open(fname) as f:
		filter_dict = {
			'plain':(lambda pattern, string: pattern==string),
			'regex':(lambda pattern, string: re.fullmatch(pattern, string)!=None)
		}
		for entry in json.load(f):
			if filter_dict[entry['type']](entry['command'], command):
				return True
	return False

def check_all(fnames, command):
	for fname in fnames:
		if check(fname, command):
			return True
	return False

def log(fname, command):
	with open(fname, 'a') as f:
		json.dump({
			'date':dt.datetime.now().isoformat('T', 'seconds'),
			'type':'plain',
			'command':command
		}, f)
		print(file=f)

def main(cmdfiles, logfile=None):
	command = os.environ.get('SSH_ORIGINAL_COMMAND', '')
	if check_all(cmdfiles, command):
		return os.system(command)
	else:
		print('Permission denied (command list)', file=sys.stderr)
		if logfile:
			log(logfile, command)
		return 1

if __name__=='__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('cmdfiles', type=str, nargs='*')
	parser.add_argument('-l', '--logfile', type=str, default=None)
	args = parser.parse_args()
	
	sys.exit(main(args.cmdfiles, args.logfile))

